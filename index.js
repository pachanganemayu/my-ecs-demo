const express = require('express');
const bodyParser = require('body-parser');

const PORT = 8080;

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  return res.send("Happy New Year to all..of you");
})

app.listen(PORT, () => {
  console.log('Server listening on port: ' + PORT);
});